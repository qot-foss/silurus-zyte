(ns silurus-zyte.store
  (:require [qot.clj-zyte-api :as api]
            [silurus.store :as st]
            [promesa.core :as p]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [medley.core :as med]))

(defn sanitize-exception [e]
  {:data (-> (ex-data e)
             (med/dissoc-in [:env :components] [:env :store] [:node]))
   :message (ex-message e)
   :type (str (type e))
   :cause (when-let [cause (ex-cause e)]
            (dissoc (sanitize-exception cause) :stacktrace))
   :stacktrace (str e)})

(defn coerce-error-node [err]
  (-> err
      (dissoc :result)
      (dissoc :parsed)
      (update :exception sanitize-exception)
      (assoc :_key (:ident err))))

(defn commit-nodes!
  [client env parent-act-node nodes & _]
  (let [frontier (-> env :config :store :frontier)
        coll (-> env :config :store :collection)
        run-id (-> env :config :run-id)
        slot run-id
        coll-type (or (-> env :config :store :collection-type) "s")
        all-nodes (map (fn [n] (st/prepare-node-for-commit env parent-act-node n)) nodes)
        {:keys [act data]} (group-by :node all-nodes)
        sort-type (or (-> env :config :strategy) :breadth-first)
        act-nodes (map (fn [n] (let [prio (case sort-type
                                            :breadth-first (:depth n)
                                            :depth-first (- 1000 (:depth n))
                                            :random (rand-int 100))]
                                 {:fingerprint (:ident n)
                                  :priority prio
                                  :queue-data n
                                  :fingerprint-data n}))
                       act)
        data-nodes (map (fn [n] (assoc n :_key (:ident n))) data)
        frontier-coords {:frontier frontier :slot slot}
        error-coll (str (str/replace run-id #"\W+" "") "Failures")
        batch-id (:zyte/batch-id parent-act-node)
        actions-res (for [n act-nodes]
                      (:requests-added (api/hcf-add-requests client frontier-coords [n])))
        act-added (reduce + actions-res)]
    (log/infof "Adding %d action nodes (max) and %d data nodes." act-added (count data-nodes))
    (when (seq data-nodes)
      (api/coll-upsert-records client {:collection coll :type coll-type} data-nodes))
    (when (= (:status parent-act-node) :failure)
      (api/coll-upsert-records client {:collection error-coll :type "s"}
                               [(coerce-error-node parent-act-node)]))
    (when batch-id
      (api/hcf-delete-batch-requests client frontier-coords [batch-id]))
    (p/promise [env nil])))

(defn checkout-next-action! [client env]
  (let [frontier (-> env :config :store :frontier)
        run-id (-> env :config :run-id)
        slot run-id
        {:keys [batch-id requests]}  (first
                                      (api/hcf-get-batch-requests client
                                                                  {:frontier frontier :slot slot} {:limit 1}))
        {:keys [queue-data]} (first requests)
        node (some-> queue-data
                     (assoc :zyte/batch-id batch-id)
                     (update :node keyword))]
    node))

(defn make-store [{:keys [store] :as config}]
  (let [{:keys [project-id api-key]} store
        client (api/make-client {:project-id project-id
                                 :api-key api-key})]
    (with-meta client
      {`st/checkout-next-action! checkout-next-action!
       `st/commit-nodes! commit-nodes!})))
