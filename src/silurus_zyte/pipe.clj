(ns silurus-zyte.pipe
  (:require [cheshire.core :as json]
            [clojure.java.io :as io])
  (:import [java.io BufferedWriter]))

(defn make-named-pipe [path]
  (io/make-writer path {}))

(defn write-to-pipe! [^BufferedWriter pipe s]
  (.write pipe s)
  (.write pipe "\n")
  (.flush pipe)
  pipe)

(defn write [ctx s]
  (if-let [pipe (-> ctx :components :shub-pipe)]
    (do
      (write-to-pipe! pipe s)
      ctx)
    (let [new-pipe (make-named-pipe (System/getenv "SHUB_FIFO_PATH"))]
      (write-to-pipe! new-pipe s)
      (assoc-in ctx [:components :shub-pipe] new-pipe))))

(defn write-item [ctx item]
  (write ctx (format "ITM %s" (json/encode item))))

(defn write-hato-request [ctx {:keys [status request request-time uri headers]}]
  (let [data {:status status
              :method (or (:method request) :GET)
              :url (:url request)
              :duration request-time
              :rs (Long/parseLong (get headers "content-length"))}]
    (write ctx (format "REQ %s" (json/encode data)))))

(defmulti write-request #(-> % :act-node :execution-log :action :name))

(defmethod write-request :silurus.fetchers.hato/request [ctx]
  (write-hato-request ctx (-> ctx :act-node :result)))

(def shub-request-logger-interceptor
  {:name ::request-logger-interceptor
   :leave (fn [ctx] (write-request ctx))})

(defn write-stats [ctx kvs]
  (write ctx (format "STA %s" (json/encode {:stats kvs}))))

(defn write-end [ctx & [msg]]
  (write ctx (format "FIN %s" (json/encode {:outcome (or msg "finished")}))))

(def log-level->int
  {:debug    10
   :info     20
   :warning  30
   :error    40
   :critical 50})

(defn shub-timbre-appendder [& [pipe]]
  (let [pip (or pipe (make-named-pipe (System/getenv "SHUB_FIFO_PATH")))]
    {:enabled? true
     :async? false
     :min-level :info
     :output-fn
     (fn [{:keys [instant level ?ns-str msg_]}]
       (let [score (log-level->int level 30)]
         (format "LOG %s" (json/encode {:level score :message (format "%s - %s" ?ns-str msg_)}))))
     :fn
     (fn [{:keys [output_]}]
       (write-to-pipe! pip output_))}))
